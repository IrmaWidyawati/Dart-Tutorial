class Manager{
  String? name;

  void sayHello(String name){
    print('Hello $name, my name is ${this.name}');
  } 
}
class VicePresident extends Manager{}
void main(){
  var manager = Manager();
  manager.name = 'Ricky';
  manager.sayHello('Irma');

  var vp = VicePresident();
  vp.name = 'Anto';
  vp.sayHello('Irma');
}

