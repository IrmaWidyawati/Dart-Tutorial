import 'data/repository_abstract_class.dart';

void main(){
  var repository = Repository('products'); 
  
  repository.id('1');
  repository.name('Printer');
  repository.quantity(200);
  repository.location('Jakarta');
}