class Application{
  static final String name = 'Belajar Dart OOP'; 
  static final String author = 'Eko Kurniawan';
  //disarankan pada saat membuat static field, agar diset sebagai final agar tidak bisa diubah oleh orang lain
}

void main(){
  print(Application.name);
  print(Application.author);
}