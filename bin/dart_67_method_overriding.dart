class Manager{
  String? name;

  void sayHello(String name){
    print('Hello $name, I am the manager, my name is ${this.name}');
  } 
}
class VicePresident extends Manager{
  void sayHello(String name){
    print('Hello $name, I am the VP, my name is ${this.name}');
  } 
}
void main(){
  var manager = Manager();
  manager.name = 'Ricky';
  manager.sayHello('Irma');

  var vp = VicePresident();
  vp.name = 'Anto';
  vp.sayHello('Irma');
}

