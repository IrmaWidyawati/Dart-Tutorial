class CatClass{
  String name = 'Hiro';
  String? age;
  final String warna = 'Hitam Putih';
}

void main(){
  var cat1 = CatClass();
  //cat1.name=null; error karena field name non nullable
  cat1.name='Blackicu';
  cat1.age='1 year';
  //cat1.warna='Hitam'; error karena field warna berupa final, tidak bisa diubah

  print(cat1.name);
  print(cat1.age);
  print(cat1.warna);

  }