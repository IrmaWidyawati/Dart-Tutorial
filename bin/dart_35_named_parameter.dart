void sayHello({String? firstName, String? lastName}){
  print('Hello $firstName $lastName'); 
}

void main(){
  sayHello();
  sayHello(firstName:'Mamoru');
  sayHello(lastName:'Maru');
  sayHello(lastName:'Maru', firstName: 'Mamoru');
  //sayHello('Mamoru','Maru'); //error: harus menyebutkan nama parameternya 
}