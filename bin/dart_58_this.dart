class CatClass{
  String name = 'Hiro';
  String? age;
  String color = 'Hitam Putih';

  CatClass(String name, String color){
    this.name = name;
    this.color = color;
  }
}

void main(){
  var cat1 = CatClass('Mamoru', 'Orange');
  print(cat1.name);
  print(cat1.color);
}