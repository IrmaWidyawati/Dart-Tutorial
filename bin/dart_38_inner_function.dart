void main(){

  void sayHello(){
    print('Hello Inner Function');

    void sayHelloAgain(){
      print('Hello again');
    }

    sayHelloAgain();
  }

  sayHello();
  //sayHelloAgain; //error undefined
}