void sayHello({required String firstName, String lastName='Default'}){
  print('Hello $firstName $lastName'); 
}

void main(){
  //sayHello(); //terjadi error, karena parameter firstname wajib diisi
  sayHello(firstName:'Mamoru');
  //sayHello(lastName:'Maru'); //terjadi error, karena parameter firstname wajib diisi
  sayHello(lastName:'Maru', firstName: 'Mamoru');
  //sayHello('Mamoru','Maru'); //error: harus menyebutkan nama parameternya 
}