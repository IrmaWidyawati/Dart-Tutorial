class CatClass{
  String name = 'Hiro';
  String? age;
  String color = 'Hitam Putih';

  CatClass(String paramName, String paramColor){
    name = paramName;
    color = paramColor;
  }
}

void main(){
  var cat1 = CatClass('Mamoru', 'Orange');
  print(cat1.name);
  print(cat1.color);
}