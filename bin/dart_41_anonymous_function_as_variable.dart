void main(){

  var upperVar = (String name){
    return name.toUpperCase();
  };

  var lowerVar = (String name) => name.toLowerCase();

  var result1 = upperVar('Belajar Dart');
  print(result1);

  var result2 = lowerVar('Belajar Dart');
  print(result2);

}