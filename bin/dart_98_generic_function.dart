import 'helper/array_helper.dart';

void main(){
  var numbers = [1,2,3,4,5,6,7,8];
  var fruits = ["Water Melon","Apple","Grape"];

  print(ArrayHelper.count(numbers));
  print(ArrayHelper.count<String>(fruits));
}