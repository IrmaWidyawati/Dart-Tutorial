int factorialRecursive(int value){
  if (value==1) {
    return 1;
  } else{
    return value * factorialRecursive(value-1);
  }
}

void main(){
  //factorialRecursive(10);
  //fact(10) => 10 * fact(9) => 10 * 9 * fact(8) => 10 * 9 * 8 * fact(7) => ....
  print(factorialRecursive(12));
 // print(10*9*8*7*6*5*4*3*2*1);
}