int factorialLoop(int value){
    var result = 1;

    for(var i = 1; i<=value; i++){
        result *= i;
    }
    return result;
}

void main(){
    print(factorialLoop(10));
    print(10*9*8*7*6*5*4*3*2*1);
}