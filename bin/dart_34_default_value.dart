void sayHello(String firstName, [String middleName='', String lastName='']){
  print('Hello $firstName $middleName $lastName'); 
}

void main(){
  sayHello('Mamoru');
  sayHello('Mamoru','Moru');
  sayHello('Mamoru','Moru','Maru');
}