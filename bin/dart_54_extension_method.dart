class CatClass{
  //void play(){
  //print ('Hiro is playing a mouse');
  //
  
  void play() => print('Hiro loves chasing a mouse');
  void sleep() => print('Hiro is sleeping now');

  // String eat(){
  //   return 'a fish';
  // }
  
  String eat() => 'a fish';
  }

  extension GaveBirthOnCatClass on CatClass{
   void  gaveBirth(String paramName){
     print('Fuku gave birth to daughter called $paramName');
   }
  }

void main(){
  var cat1 = CatClass();
  var cat2 = CatClass();
  var food = cat1.eat();
  cat1.play();
  cat1.sleep();
  print('Hiro is going to eat $food');
  
  cat2.gaveBirth('Kichi');


}

