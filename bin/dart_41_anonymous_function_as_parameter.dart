void sayHello(String name, String Function(String) filter){
  print('KonSnichiwa ${filter(name)}');
}

void main(){
  sayHello ('Midori Yoshinaga', (name){
    return name.toUpperCase();
  });

  sayHello('Midori Yoshinaga', (name) => name.toLowerCase());

}
