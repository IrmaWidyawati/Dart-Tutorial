import 'data/product.dart';

void main(){
  var product = Product();
  product.id = '1';
  product.name = 'Laptop';
  //product._getQuantity(); //error the method _getQuantity isn't define for the type Product
  //product._quantity = 100; //error the setter '_quantity isn't defined for the type of product
}