class CatClass{
  String name = 'Hiro';
  String color = 'Hitam Putih';
  CatClass(this.name, this.color);
  CatClass.withName(String name):this(name,"Brown");
  CatClass.withColor(String color):this("Kichi",color);
  CatClass.noName() : this.withName("No Name");
  CatClass.noColor() : this.withColor("No Color");

  }
void main(){
  var cat4 = CatClass.noName();
  print(cat4.name); print(cat4.color);

  var cat5 = CatClass.noColor();
  print(cat5.name); print(cat5.color);



}