class User{
  String? username;
  String? name;
  String? email;
}

User? createUser(){
  return null;
}
void main(){
  //tanpa cascade notation
  // var user = User();
  // user.username = 'MinkyDinky';
  // user.name = 'Minky';
  // user.email= 'MinkyDinky@Twinky.com';

  //menggunakan cascade notation non Nullable
  var user = User()
    ..username = 'MinkyDinky'
    ..name     = 'Minky'
    ..email    = 'MinkyDinky@Twinky.com';

  //menggunakan cascade notation Nullable
  User? user2 = createUser()
  ?..username = 'Pie'
  ..name      = 'Pinky'
  ..email     = 'PinkyPie@Twinky.com'; 

  print(user.username);
  print(user.name);
  print(user.email);

  print(user2?.username);
  print(user2?.name);
  print(user2?.email);

}