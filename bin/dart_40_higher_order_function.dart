void sayHello(String name, String Function(String) filter){
  var filteredName=filter(name);
  print('Hi $filteredName');
}

String filterBadWord(String name){
  if (name=='jelek'){
    return '*****'; 
  } else {
    return name;
  }
} 

void main(){
  sayHello('Irma', filterBadWord);
  sayHello('jelek', filterBadWord);
}