import 'data/mydata.dart';
void check(dynamic data){
  if(data is MyData<String>){
    print('MyData<String>');
  }else if(data is MyData<num>){
    print('MyData<num>');
  }else if(data is MyData<bool>){
    print('MyData<bool>');
  }else { print('Others');}
}
void main(){
  check(MyData('Printer'));
  check(MyData(30));
  check(MyData(true));
  check('Printer');
  check(30);
  check(true);
}