class CatClass{
  String name = 'Hiro';
  String color = 'Hitam Putih';
  CatClass(this.name, this.color);
  CatClass.withName(this.name);
  CatClass.withColor(this.color);}
void main(){
  var cat1 = CatClass('Mamoru', 'Orange');
  print(cat1.name); print(cat1.color);

  var cat2 = CatClass.withName('Sasongki');
  print(cat2.name); print(cat2.color);

  var cat3 = CatClass.withColor('Grey');
  print(cat3.name); print(cat3.color);


}